

const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo instance reviewSchema từ Class Schema
const CustomerSchema = new Schema({
    fullName: {
        type: String,
        required:true,
    },
    phone: {
        type: String,
        required:true,
        unique:true
    },
    email:{
        type: String,
        required:true,
        unique:true
    },
    address:{
        type:String,
        default:""
    },
    city:{
        type:String,
        default:""
    },
    country:{
        type:String,
        default:"" 
    },
    orders:[{
        type: mongoose.Types.ObjectId,
        ref:"Order"      
    }],
}, {
    // Ghi dấu bản ghi được tạo hay cập nhật vào thời gian nào
    timestamps: true
});

// Biên dịch Review Model từ reviewSchema
module.exports = mongoose.model("Customer", CustomerSchema);