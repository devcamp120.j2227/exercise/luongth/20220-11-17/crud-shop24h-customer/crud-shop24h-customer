
const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;
//Khởi tạo instance reviewSchema từ Class Schema
const OrderDetailSchema = new Schema({
    product: [{
        type: mongoose.Types.ObjectId,
        ref:"Product"      
    }],
    quantity: {
        type:Number,
        default:0
    }
}, {
    // Ghi dấu bản ghi được tạo hay cập nhật vào thời gian nào
    timestamps: true
});
// Biên dịch Review Model từ reviewSchema
module.exports = mongoose.model("OrderDetail", OrderDetailSchema);