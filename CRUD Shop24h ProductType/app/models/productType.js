const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo instance reviewSchema từ Class Schema
const ProductTypeSchema = new Schema({
    name: {
        type: String,
        require:true,
        unique:true
    },
    description: {
        type: String  
    }
}, {
    // Ghi dấu bản ghi được tạo hay cập nhật vào thời gian nào
    timestamps: true
});

// Biên dịch Review Model từ reviewSchema
module.exports = mongoose.model("ProductType", ProductTypeSchema);