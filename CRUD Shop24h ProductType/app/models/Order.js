


const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo instance reviewSchema từ Class Schema
const OrderSchema = new Schema({
    orderDate: {
        type: Date,
          // `Date.now()` returns the current unix timestamp as a number
          default: Date.now

    },
    shippedDate: {
        type: Date
    },
    note:{
        type: String,
    },
    orderDetails:[{
        type: mongoose.Types.ObjectId,
        ref:"OrderDetail"      
    }],
    cost:{
        type:Number,
        default:0
    }
}, {
    // Ghi dấu bản ghi được tạo hay cập nhật vào thời gian nào
    timestamps: true
});

// Biên dịch Review Model từ reviewSchema
module.exports = mongoose.model("Order", OrderSchema);