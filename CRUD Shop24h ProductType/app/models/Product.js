
const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo instance reviewSchema từ Class Schema
const Productchema = new Schema({
    name: {
        type:String,
        required:true,
        unique:true
    },
    description: {
        type:String,
    },
    type:[{
        type:mongoose.Types.ObjectId,
        required:true,
        ref:"ProductType"
    }],
    imageUrl:{
        type:String,
        required:true,   
    },
    buyPrice:{
        type:Number,
        required:true,   
    },
    promotionPrice:{
        type:Number,
        required:true,   
    },
    amount:{
        type:Number,
        default:0 
    },
}, {
    // Ghi dấu bản ghi được tạo hay cập nhật vào thời gian nào
    timestamps: true
});

// Biên dịch Review Model từ reviewSchema
module.exports = mongoose.model("Product", Productchema);