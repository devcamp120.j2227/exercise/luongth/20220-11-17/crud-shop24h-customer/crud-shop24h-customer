
// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module Drink Model
const proDuctypeModel = require("../models/productType");
const  productModel = require("../models/Product");
const getAllProducTypeModel = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    proDuctypeModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all Drink successfully",
            data: data
        })
    })
}

const createProducType = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const productId = request.params.productId;
    const body = request.body;
    console.log(body)
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "productId ID không hợp lệ"
        })
    };
    //Kiểm tra maNuocuong có hợp lệ hay không
    if(!body.name) {
        return response.status(400).json({
            status: "Bad Request",
            message: "name không hợp lệ"
        })
    }
     //Kiểm tra maNuocuong có hợp lệ hay không
    if(!body.description) {
        return response.status(400).json({
            status: "Bad Request",
            message: "description không hợp lệ"
        })
    }
    

    // B3: Gọi Model tạo dữ liệu
    const newProducType = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
       
    }

    proDuctypeModel.create(newProducType, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        productModel.findByIdAndUpdate(productId, {
            $push: {
                type: data._id
            }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create ProducType Successfully",
                data: data
            })
        })
    })
};
const getAllProducTypeOfProduct = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const ProductId = request.params.productId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(ProductId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Course ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    productModel.findById(ProductId)
        .populate("type")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Get all producType of Product successfully",
                data: data
            })
        })
}

const getProductypeById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const ProductypeId = request.params.productypeId;
    
    const orderdetailId = request.params.orderdetailId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderdetailId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkID không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(ProductypeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkID không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    proDuctypeModel.findById(ProductypeId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        // Sau khi xóa xong 1 reivew khỏi collection cần cóa thêm reviewID trong course đang chứa nó
        productModel.findByIdAndUpdate(orderdetailId, {
            $pull: { product: OrderId }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(204).json({
                status: "Delete orderdetailId successfully"
            })
        })
    })
}

const updateProducTypeId = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const ProductypeId = request.params.productypeId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(ProductypeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ProductypeId không hợp lệ"
        })
    }

    if(body.name !== undefined && body.name.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Name không hợp lệ"
        })
    }
    if(body.description !== undefined && body.description.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "description không hợp lệ"
        })
    }
    

    // B3: Gọi Model tạo dữ liệu
    const updateProducType = {}

    if(body.description !== undefined) {
        updateProducType.description = body.description
    }

    if(body.name !== undefined) {
        updateProducType.name = body.name
    }

    proDuctypeModel.findByIdAndUpdate(ProductypeId, updateProducType, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update updateProducType successfully",
            data: data
        })
    })
}

const deleteProducTypeByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const ProductypeId = request.params.productypeId;
    const ProductId = request.params.productId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(ProductypeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    proDuctypeModel.findByIdAndDelete(ProductypeId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        // Sau khi xóa xong 1 reivew khỏi collection cần cóa thêm reviewID trong course đang chứa nó
        productModel.findByIdAndUpdate(ProductId, {
            $pull: { type: ProductypeId }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(204).json({
                status: "Delete orderdetailId successfully"
            })
        })
    })
}

module.exports = {
    getAllProducTypeModel,
    createProducType,
    getProductypeById,
    getAllProducTypeOfProduct,
    updateProducTypeId,
    deleteProducTypeByID
}