

// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module Course Model
const productModel = require("../models/Product");
const orderdetailModel = require("../models/OrderDetail");

const getAllProduct = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    productModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all course successfully",
            data: data
        })
    })
}

const createProduct = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderdetailId = request.params.orderdetailId;

    const body = request.body;
    
   // B2: Validate dữ liệu
   if (!mongoose.Types.ObjectId.isValid(orderdetailId)) {
    return response.status(400).json({
        status: "Bad Request",
        message: "orderdetailId ID không hợp lệ"
    })
};

if(!body.name) {
    return response.status(400).json({
        status: "Bad Request",
        message: "name không hợp lệ"
    })
}
if(!body.description) {
    return response.status(400).json({
        status: "Bad Request",
        message: "description không hợp lệ"
    })
}
if(!body.description) {
    return response.status(400).json({
        status: "Bad Request",
        message: "description không hợp lệ"
    })
}
if(!body.imageUrl) {
    return response.status(400).json({
        status: "Bad Request",
        message: "imageUrl không hợp lệ"
    })
}
if(!body.buyPrice) {
    return response.status(400).json({
        status: "Bad Request",
        message: "buyPrice không hợp lệ"
    })
}
if(!body.promotionPrice) {
    return response.status(400).json({
        status: "Bad Request",
        message: "promotionPrice không hợp lệ"
    })
}
if(!body.amount) {
    return response.status(400).json({
        status: "Bad Request",
        message: "amount không hợp lệ"
    })
}
    // B3: Gọi Model tạo dữ liệu
    const newCourse = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
    }
    console.log(newCourse)
     productModel.create(newCourse, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        orderdetailModel.findByIdAndUpdate(orderdetailId, {
            $push: {
                product : data._id
            }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create Product Successfully",
                data: data
            })
        })
    })
};

const getAllProductlOfOrderdetail = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderdetailId = request.params.orderdetailId;
   
    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderdetailId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderId ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    orderdetailModel.findById(orderdetailId)
        .populate("product")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Get all producType of Product successfully",
                data: data
            })
        })
}

const getProductById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const productId = request.params.productId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "productId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
     productModel.findById(productId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail course successfully",
            data: data
        })
    })
}

const updateProductById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const productId = request.params.productId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    if(body.bodyTitle !== undefined && body.bodyTitle.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Product không hợp lệ"
        })
    }

    if(body.bodyNoStudent !== undefined && ( isNaN(body.bodyNoStudent) || body.bodyNoStudent < 0 )) {
        return response.status(400).json({
            status: "Bad Request",
            message: "No Product không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateProduct = {}

    if(body.name !== undefined) {
        updateCourse.name = body.name
    }

    if(body.description !== undefined) {
        updateCourse.description = body.description
    }

    if(body.imageUrl !== undefined) {
        updateCourse.imageUrl = body.imageUrl
    }

    if(body.buyPrice !== undefined) {
        updateCourse.buyPrice = body.buyPrice
    }
    if(body.promotionPrice !== undefined) {
        updateCourse.promotionPrice = body.promotionPrice
    }

    if(body.amount !== undefined) {
        updateCourse.amount = body.amount
    }
    productModel.findByIdAndUpdate(productId, updateProduct, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update course successfully",
            data: data
        })
    })
}

const deleteProductByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const productId = request.params.productId;
    const OrderdetailId = request.params.orderdetailId;

    if(!mongoose.Types.ObjectId.isValid(productId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "productId không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(OrderdetailId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderdetailId không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    productModel.findByIdAndDelete(productId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        // Sau khi xóa xong 1 reivew khỏi collection cần cóa thêm reviewID trong course đang chứa nó
        orderdetailModel.findByIdAndUpdate(OrderdetailId, {
            $pull: { orderDetails: productId }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(204).json({
                status: "Delete OrderdetailId successfully"
            })
        })
    })
}

module.exports = {
    getAllProduct,
    createProduct,
    getProductById,
    getAllProductlOfOrderdetail,
    updateProductById,
    deleteProductByID
}