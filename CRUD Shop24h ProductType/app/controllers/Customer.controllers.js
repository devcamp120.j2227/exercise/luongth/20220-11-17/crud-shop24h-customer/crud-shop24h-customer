

// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module Course Model
const CustomerModel = require("../models/Customer");

const getAllCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    CustomerModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all course successfully",
            data: data
        })
    })
}

const createCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;

    // B2: Validate dữ liệu
    if(!body.fullName) {
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
        })
    }
    if(!body.phone) {
        return response.status(400).json({
            status: "Bad Request",
            message: "phone không hợp lệ"
        })
    }
    if(!body.email) {
        return response.status(400).json({
            status: "Bad Request",
            message: "email không hợp lệ"
        })
    }
    if(!body.address) {
        return response.status(400).json({
            status: "Bad Request",
            message: "address không hợp lệ"
        })
    }
    if(!body.city) {
        return response.status(400).json({
            status: "Bad Request",
            message: "city không hợp lệ"
        })
    }
    if(!body.country) {
        return response.status(400).json({
            status: "Bad Request",
            message: "country không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newCustomer = {
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,

    }
    console.log(newCustomer)
     CustomerModel.create(newCustomer, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status: "Create newCustomer successfully",
            data: data
        })
    })
}

const getCustomerById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const CustomerId = request.params.customerId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "productId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
     CustomerModel.findById(CustomerId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail customer successfully",
            data: data
        })
    })
}

const updateCustomerById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const CustomerId = request.params.customerId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    if(body.phone !== undefined && body.phone.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "phone không hợp lệ"
        })
    }
    if(body.fullName !== undefined && body.fullName.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
        })
    }
    if(body.email !== undefined && body.email.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "email không hợp lệ"
        })
    }
    if(body.address !== undefined && body.address.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
        })
    }
    if(body.city !== undefined && body.city.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "city không hợp lệ"
        })
    }
    if(body.country !== undefined && body.country.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "country không hợp lệ"
        })
    }
   
    // B3: Gọi Model tạo dữ liệu
    const updateCustomer = {}

    if(body.fullName !== undefined) {
        updateCustomer.fullName = body.fullName
    }

    if(body.phone !== undefined) {
        updateCustomer.phone = body.phone
    }

    if(body.email !== undefined) {
        updateCustomer.email = body.email
    }

    if(body.address !== undefined) {
        updateCustomer.address = body.address
    }
    if(body.city !== undefined) {
        updateCustomer.city = body.city
    }

    if(body.country !== undefined) {
        updateCustomer.country = body.country
    }
    productModel.findByIdAndUpdate(CustomerId, updateCustomer, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update customer successfully",
            data: data
        })
    })
}

const deleteCustomerByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const CustomerId = request.params.customerId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CustomerId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    CustomerModel.findByIdAndDelete(productId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete CustomerId successfully"
        })
    })
}

module.exports = {
    getAllCustomer,
    createCustomer,
    getCustomerById,
    updateCustomerById,
    deleteCustomerByID
}
