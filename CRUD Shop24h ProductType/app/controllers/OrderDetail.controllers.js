
// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module Drink Model
const OrderDetailModel = require("../models/OrderDetail");
const  OrderModel = require("../models/Order");
const getAllOrderDetail = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    OrderDetailModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all Drink successfully",
            data: data
        })
    })
}

const createOrderDetail = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderId = request.params.orderId;
    const body = request.body;
    console.log(body)
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderId ID không hợp lệ"
        })
    }
     //Kiểm tra quantity có hợp lệ hay không
    if(!body.quantity) {
        return response.status(400).json({
            status: "Bad Request",
            message: "quantity không hợp lệ"
        })
    }
    
    // B3: Gọi Model tạo dữ liệu
    const newOrderDetail = {
        _id: mongoose.Types.ObjectId(),
        quantity: body.quantity,
       
    }
    OrderDetailModel.create(newOrderDetail, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        OrderModel.findByIdAndUpdate(OrderId, {
            $push: {
                orderDetails : data._id
            }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create OrderDetailModel Successfully",
                data: data
            })
        })
    })
};
const getAllOrderDetailOfOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderId = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderId ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    OrderModel.findById(OrderId)
        .populate("orderDetails")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Get all producType of Product successfully",
                data: data
            })
        })
}

const getOrderDetialById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderdetailId = request.params.orderdetailId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(OrderdetailId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderdetaillId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    OrderDetails.findById(OrderdetailId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get OrderDetails successfully",
            data: data
        })
    })
}

const updateOrderDetailById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderdetailId = request.params.orderdetailId;

    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(ProductypeId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ProductypeId không hợp lệ"
        })
    }
    if(body.quantity !== undefined && body.quantity.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "quantity không hợp lệ"
        })
    }
    

    // B3: Gọi Model tạo dữ liệu
    const updateOrderDetail = {}

    if(body.quantity !== undefined) {
        updateProducType.quantity = body.quantity
    }

    proDuctypeModel.findByIdAndUpdate(OrderdetailId, updateOrderDetail, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update updateOrderDetail successfully",
            data: data
        })
    })
}

const deleteOrderDetailByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderdetailId = request.params.orderdetailId;
    const OrderId = request.params.orderId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(OrderdetailId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderdetailId không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderId không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    OrderDetailModel.findByIdAndDelete(OrderdetailId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        // Sau khi xóa xong 1 OrderdetailId khỏi collection cần cóa thêm reviewID trong course đang chứa nó
       OrderModel.findByIdAndUpdate(OrderId, {
            $pull: { product: OrderdetailId }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(204).json({
                status: "Delete ProductypeId successfully"
            })
        })
    })
}

module.exports = {
    getAllOrderDetail,
    createOrderDetail,
    getOrderDetialById,
    getAllOrderDetailOfOrder,
    updateOrderDetailById,
    deleteOrderDetailByID
}