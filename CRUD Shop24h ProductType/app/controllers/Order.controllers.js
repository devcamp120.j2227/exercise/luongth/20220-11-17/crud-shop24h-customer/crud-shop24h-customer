



// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module Course Model
const OrderModel = require("../models/Order");
const customerModel = require("../models/Customer");

const getAllOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    OrderModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all course successfully",
            data: data
        })
    })
}

const createOrder = (request, response) => {

    // B1: Chuẩn bị dữ liệu
    const CustomerId = request.params.customerId;

    const body = request.body;
    
     // B2: Validate dữ liệu
     if(!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderId không hợp lệ"
        })
    }

     if(!body.note) {
        return response.status(400).json({
            status: "Bad Request",
            message: "note không hợp lệ"
        })
    }
    if(!body.cost) {
        return response.status(400).json({
            status: "Bad Request",
            message: "cost không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newOrder = {
        _id: mongoose.Types.ObjectId(),
        note: body.note,
        cost: body.cost,
       
    }
    console.log(newOrder)
     OrderModel.create(newOrder, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        customerModel.findByIdAndUpdate(CustomerId, {
            $push: {
                orders: data._id
            }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create orders Successfully",
                data: data
            })
        })
    })
}

const getOrderById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderId = request.params.orderId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
     OrderModel.findById(OrderId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail OrderId successfully",
            data: data
        })
    })
}

const getAllOrderOfCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const CustomerId = request.params.customerId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CustomerId ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    customerModel.findById(CustomerId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Get all producType of Product successfully",
                data: data
            })
        })
}

const updateOrderById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderId = request.params.orderId;

    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    if(body.note !== undefined && body.note.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "note không hợp lệ"
        })
    }

    if(body.cost !== undefined && ( isNaN(body.cost) || body.cost < 0 )) {
        return response.status(400).json({
            status: "Bad Request",
            message: "No cost không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateOrder = {}

    if(body.cost !== undefined) {
        updateCourse.cost = body.cost
    }

    if(body.note !== undefined) {
        updateCourse.note = body.note
    }

    OrderModel.findByIdAndUpdate(OrderId, updateOrder, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update course successfully",
            data: data
        })
    })
}

const deleteProducTypeByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderId = request.params.orderId;
    const CustomerId = request.params.customerId;
  
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderId không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(CustomerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CustomerId không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    OrderModel.findByIdAndDelete(OrderId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        // Sau khi xóa xong 1 reivew khỏi collection cần cóa thêm reviewID trong course đang chứa nó
        customerModel.findByIdAndUpdate(CustomerId, {
            $pull: { orders: OrderId }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(204).json({
                status: "Delete ProductypeId successfully"
            })
        })
    })
}

module.exports = {
    getAllOrder,
    createOrder,
    getOrderById,
    getAllOrderOfCustomer,
    updateOrderById,
    deleteProducTypeByID
}