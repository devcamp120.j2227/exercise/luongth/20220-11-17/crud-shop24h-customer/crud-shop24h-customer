

// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
//const courseMiddleware = require("../middlewares/courseMiddleware");

// Import course controller
const orderDetailController = require("../controllers/OrderDetail.controllers")

router.get("/orderdetails",  orderDetailController.getAllOrderDetail);

router.post("/orders/:orderId/orderdetails",  orderDetailController.createOrderDetail);

router.get("/orders/:orderId/orderdetails",  orderDetailController.getAllOrderDetailOfOrder);

 router.put("/orderdetails/:orderdetailId", orderDetailController.updateOrderDetailById)

router.get("/orderdetails/:orderdetailId", orderDetailController.getOrderDetialById)


router.delete("/orders/:orderId/orderdetails/:orderdetailId",  orderDetailController.deleteOrderDetailByID)

module.exports = router;

