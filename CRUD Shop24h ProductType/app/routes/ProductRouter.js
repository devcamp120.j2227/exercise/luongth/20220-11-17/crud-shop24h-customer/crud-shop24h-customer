// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
//const courseMiddleware = require("../middlewares/courseMiddleware");

// Import course controller
const productController = require("../controllers/Product.controllers")

router.get("/products",  productController.getAllProduct);

router.post("/orderdetails/:orderdetailId/products",  productController.createProduct);

router.get("/orderdetails/:orderdetailId/products",  productController.getAllProductlOfOrderdetail);

router.get("/products/:productId", productController.getProductById)


router.put("/products/:productId", productController.updateProductById)

router.delete("/orderdetails/:orderdetailId/products/:productId",  productController.deleteProductByID)

module.exports = router;
