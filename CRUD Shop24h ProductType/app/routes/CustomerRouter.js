

// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
//const courseMiddleware = require("../middlewares/courseMiddleware");

// Import course controller
const customerController = require("../controllers/Customer.controllers")

router.get("/customers",  customerController.getAllCustomer);

router.post("/customers",  customerController.createCustomer);

router.get("/customers/:customerId",  customerController.getCustomerById);


router.put("/customers/:customerId", customerController.updateCustomerById)

router.delete("/customer/:customerId",  customerController.deleteCustomerByID)

module.exports = router;
