
// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
//const courseMiddleware = require("../middlewares/courseMiddleware");

// Import course controller
const producTypeController = require("../controllers/ProductType.controller")

router.get("/products",  producTypeController.getAllProducTypeModel);

router.post("/products/:productId/productype",  producTypeController.createProducType);

router.get("/products/:productId/productype",  producTypeController.getAllProducTypeOfProduct);


router.put("/productype/:productypeId", producTypeController.updateProducTypeId)

router.delete("/products/:productId/productype/:productypeId",  producTypeController.deleteProducTypeByID)

module.exports = router;
