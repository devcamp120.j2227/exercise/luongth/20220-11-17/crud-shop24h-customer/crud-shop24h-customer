
// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
//const courseMiddleware = require("../middlewares/courseMiddleware");

// Import course controller
const orderController = require("../controllers/Order.controllers")

router.get("/orders",  orderController.getAllOrder);

router.post("/customers/:customerId/orders",  orderController.createOrder);

router.get("/customers/:customerId/orders",  orderController.getAllOrderOfCustomer);

 router.put("/orders/:orderId", orderController.updateOrderById)

router.get("/orders/:orderId", orderController.getOrderById)


router.delete("/customers/:customerId/orders/:orderId",  orderController.deleteProducTypeByID)

module.exports = router;
