// Khai báo thư viên Express
const express = require("express");
// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Khởi tạo app express
const app = express();

// Khai báo cổng chạy app 
const port = 8000;

// Khai báo router app

const productRouter = require("./app/routes/ProductRouter");
const producTypeRouter = require("./app/routes/ProducTypeRouter");
const orderdetailRouter = require("./app/routes/OrderDetailRouter");
const orderRouter = require("./app/routes/OrderRouter");
const customerRouter = require("./app/routes/CustomerRouter");

// Cấu hình request đọc được body json
app.use(express.json());

app.use((request, response, next) => {
    console.log("Current time: ", new Date());
    next();
})

app.use((request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})

mongoose.connect("mongodb://127.0.0.1:27017/test", (error) => {
    if(error) throw error;
    console.log("Connect MongoDB successfully!");
})

// Khai báo API /
app.get("/", (request, response) => {
    console.log("Call API GET /");

    response.json({
        message: "Devcamp Middleware Express APP"
    })
})
// App sử dụng router

app.use("/api", productRouter);
app.use("/api", producTypeRouter);
app.use("/api", orderdetailRouter);
app.use("/api", orderRouter);
app.use("/api", customerRouter);






// Chạy app trên cổng
app.listen(port, () => {
    console.log("App listening on port:", port);
})